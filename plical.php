<?php
# Copyright (c) 2009 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license.
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE

function add_text(&$activity, $name, $text) {
    global $doc;
    $tmp = $doc->createElement($name);
    $activity->appendChild($tmp);
    $tmp->appendChild($doc->createTextNode($text));
}
 
require_once('functions/ical_parser.php');
 
$doc = new DOMDocument();
$doc->formatOutput = true;
 
$tl = $doc->createElement("Activities");
$doc->appendChild($tl);
 
$uid_done = array();
 
echo "<table>";
 
foreach ($master_array as $day => $day_array) {
    if ($day >= date("Ymd")) {
        foreach ($day_array as $time => $time_array) {
            if ($time > 0) {
                foreach ($time_array as $uid => $event) {
                    $activity = $doc->createElement("Activity");
                    $tl->appendChild($activity);
                    add_text($activity, "ActivitySourceID", $uid."-".$day);
                    if (!$uid_done[$uid])
                        $uid_done[$uid] = $uid."-".$day;
                    else
                        add_text($activity, "LinkWithActivitySourceID", $uid_done[$uid]);
                    add_text($activity, "Name", urldecode($event["event_text"]));
                    add_text($activity, "Description", urldecode($event["description"]));
                    add_text($activity, "Starts", date("c", $event["start_unixtime"]));
                    add_text($activity, "Ends", date("c", $event["end_unixtime"]));
                    add_text($activity, "ContactName", "General Enquiries");
                    add_text($activity, "ContactNumber", "n/a");
 
                    $vnode = $doc->createElement("Venue");
                    $activity->appendChild($vnode);
                    add_text($vnode, "ContactPhone", "n/a");
                    add_text($vnode, "ContactForename", "General");
                    add_text($vnode, "ContactSurname", "Enquiries");
                    
                    #$event["location"] = str_replace(array("Leicester,", ")."), array("Leicester", "),"), $event["location"]);
                    #$event["location"] = preg_replace("/\(.*\)$/", "", $event["location"]);
                    $event["location"] = urldecode($event["location"]);
                    $addr = explode(",", $event["location"]);
                    echo "<tr>"; #print_r($event);
                    add_text($vnode, "ProviderVenueID", sha1(trim($addr[0])));
                    add_text($vnode, "Name", $addr[0]);
                    add_text($vnode, "BuildingNameNo", $addr[0]);
                    add_text($vnode, "Postcode", trim($addr[sizeof($addr)-1], " ."));
                    
                    if ($event["calname"]) {
                        echo "<td>" . $event["calname"] . "</td>";
                        $onode = $doc->createElement("ActivityProvider");
                        $activity->appendChild($onode);
                        add_text($onode, "DPProviderID", sha1(trim($event["calname"])));
                        add_text($onode, "Name", $event["calname"]);
                        add_text($onode, "ContactPhone", "n/a");
                        add_text($onode, "ContactForename", "General");
                        add_text($onode, "ContactSurname", "Enquiries");
                    }
                    foreach ($addr as $a) echo "<td>$a</td>";
                    echo "</tr>";
                }
            }
        }
    }
}
 
echo "</table>";
 
$doc->save("output.xml");
echo "<a href=\"output.xml\">View XML</a>";
 
?>